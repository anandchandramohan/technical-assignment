package com.fugen.logic;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlFileConversion {


	public static Document mergeFile(File fXmlFile,File sXmlFile) throws Exception {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc1 = dBuilder.parse(fXmlFile);
		Document doc2 = dBuilder.parse(sXmlFile);

		doc1.getDocumentElement().normalize();
		doc2.getDocumentElement().normalize();


		NodeList nList = doc1.getElementsByTagName("person");



		NodeList nList1 = doc2.getElementsByTagName("person");

		//creating new xml file 
		Document mainDoc = dBuilder.newDocument();
		Element personData = mainDoc.createElement("personData");
		mainDoc.appendChild(personData);


		for (int temp = 0; temp < nList.getLength(); temp++)
		{
			Node nNode = nList.item(temp);
			Element per1 = mainDoc.createElement(nNode.getNodeName());
			NamedNodeMap attrs = nNode.getAttributes();
			for(int i=0; i<attrs.getLength(); i++)
			{
				Attr attribute = (Attr)attrs.item(i); 
				per1.setAttribute( attribute.getName(), attribute.getValue());  
			}

			for (int temp1 = 0; temp1 < nList1.getLength(); temp1++) 
			{


				Node nNode1 = nList1.item(temp1);

				if (nNode.getNodeType() == Node.ELEMENT_NODE && nNode1.getNodeType() == Node.ELEMENT_NODE) 
				{


					//attributes1
					Attr attribute = (Attr)attrs.item(1);     


					NamedNodeMap attrs1 = nNode1.getAttributes();  
					//attributes1
					Attr attribute1 = (Attr)attrs1.item(1);     


					if(attribute.getValue().equals(attribute1.getValue()))

					{
						Element eElement = (Element) nNode;

						Element eElement1 = (Element) nNode1;


						Element address = mainDoc.createElement("address");
						address.appendChild(mainDoc.createTextNode(getTagValue("address", eElement)));
						per1.appendChild(address);

						Element phoneNumber = mainDoc.createElement("phonenumber");
						phoneNumber.appendChild(mainDoc.createTextNode(getTagValue("phonenumber", eElement)));
						per1.appendChild(phoneNumber);

						Element salary = mainDoc.createElement("salary");
						salary.appendChild(mainDoc.createTextNode(getTagValue("salary", eElement1)));
						per1.appendChild(salary);

						Element pension = mainDoc.createElement("pension");
						pension.appendChild(mainDoc.createTextNode(getTagValue("pension", eElement1)));
						per1.appendChild(pension);

						/*NodeList list = nNode.getChildNodes();
	                             for(int i = 0 ; i<list.getLength() ; i++)
	                             { 
	                                 System.out.println("hello");
	                                 Node n1=(Node)list.item(i);
	                                 String s1=(String)n1.getNodeName();
	                                 String s2=(String)n1.getTextContent();
	                                 Element x = mainDoc.createElement("per");
	                                x.appendChild(mainDoc.createTextNode(s2));
	                                per1.appendChild(x);
	                                 System.out.println(n1.getNodeName()+" "+n1.getTextContent());
	                                 per1.appendChild(x);
	                                 System.out.println("hello");
	                             }


	                            NodeList list1 = nNode1.getChildNodes();
	                             for(int i = 0 ; i<list1.getLength() ; i++) 
	                             { 
	                                 Node n1=(Node)list1.item(i);
	                                 String s1=(String)n1.getNodeName();
	                                 String s2=(String)n1.getTextContent();
	                                 Element x = mainDoc.createElement("per");
	                                x.appendChild(mainDoc.createTextNode(s2));
	                                per1.appendChild(x);
	                                 System.out.println(n1.getNodeName()+" "+n1.getTextContent());
	                                 per1.appendChild(x);
	                             }*/

					}
				}
			}
			personData.appendChild(per1);
		}

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();

		DOMSource source = new DOMSource(mainDoc);          
		StreamResult result = new StreamResult(System.out);  

		transformer.transform(source, result);
		displayElement(mainDoc);
		return mainDoc;
		



	}
	
	public static void displayElement(Document doc){
		
		  doc.getDocumentElement().normalize();
			 
	        System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	        NodeList nList = doc.getElementsByTagName("person");
	        System.out.println("-----------------------");
	 
	        for (int temp = 0; temp < nList.getLength(); temp++) {
	 
	           Node nNode = nList.item(temp);
	           if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
	              Element eElement = (Element) nNode;
	            
	            
	              System.out.println("Address : " + getTagValue("address", eElement));
	              System.out.println("Phone Number : " + getTagValue("phonenumber", eElement));
	              System.out.println("Salary : " + getTagValue("salary", eElement));
	              System.out.println("Pension : " + getTagValue("pension", eElement));
	           }
	 
	           }
		  
	  }
	 
     
	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}

}
