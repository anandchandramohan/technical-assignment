package com.fugen.logic;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MergeXmlDemo {

		  public static Document merge(String expression,File... files) 
				  throws Exception {
		    XPathFactory xPathFactory = XPathFactory.newInstance();
		    XPath xpath = xPathFactory.newXPath();
		    XPathExpression compiledExpression = xpath.compile(expression);
		    return merge(compiledExpression, files);
		  }

		  private static Document merge(XPathExpression expression,
		      File... files) throws Exception {
		    DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
		        .newInstance();
		    docBuilderFactory.setIgnoringElementContentWhitespace(true);
		    DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		    Document base = docBuilder.parse(files[0]);
		    
		    NodeList nodes = base.getElementsByTagName("geodata");
		    for (int i = 0; i < nodes.getLength(); i++)
		    {
		          base.renameNode(nodes.item(i), null, "persondata");
		    }

		    Node results = (Node) expression.evaluate(base,XPathConstants.NODE);
		    if (results == null) {
		      throw new IOException(files[0]
		          + ": expression does not evaluate to node");
		    }

		    for (int i = 1; i < files.length; i++) {
		      Document merge = docBuilder.parse(files[i]);
		      Node nextResults = (Node) expression.evaluate(merge,XPathConstants.NODE);
		      while (nextResults.hasChildNodes()) {
		        Node kid = nextResults.getFirstChild();
		        nextResults.removeChild(kid);
		        kid = base.importNode(kid, true);
		        results.appendChild(kid);
		      }
		    }

		    return base;
		  }

		  public static void print(Document doc) throws Exception {
		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
		    Transformer transformer = transformerFactory.newTransformer();
		    DOMSource source = new DOMSource(doc);
		    Result result = new StreamResult(System.out);
		    transformer.transform(source, result);
		  }
		  public static void displayElement(Document doc){
			  doc.getDocumentElement().normalize();
				 
		        System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		        NodeList nList = doc.getElementsByTagName("person");
		        System.out.println("-----------------------");
		 
		        for (int temp = 0; temp < nList.getLength(); temp++) {
		 
		           Node nNode = nList.item(temp);
		           if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		 
		              Element eElement = (Element) nNode;
		              
		 
		              System.out.println("Address : " + getTagValue("address", eElement));
		              System.out.println("Phone Number : " + getTagValue("phonenumber", eElement));
		              System.out.println("Salary : " + getTagValue("salary", eElement));
		              System.out.println("Pension : " + getTagValue("pension", eElement));
		           }
		 
		           }
			  
		  }
		  public static String getTagValue(String sTag, Element eElement) {
			    NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
			 
			        Node nValue = (Node) nlList.item(0);
			 
			    return nValue.getNodeValue();
			  }

		
}
