package com.fugen.bean;

public class PersonData {
	int id;
	String name;
	String address;
	long phoneNumber;
	int salary,pension;

	PersonData(int id,String name,String add,long phNo,int sal,int pen)
	{
		this.id=id;
		this.name=name;
		address=add;
		phoneNumber=phNo;
		salary=sal;
		pension=pen;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	void setAddress(String address)
	{
		this.address=address;
	}

	String getAddress()
	{
		return address;
	}

	void setPhoneNumber(long phoneNumber)
	{
		this.phoneNumber=phoneNumber;
	}

	long getPhoneNumber()
	{
		return phoneNumber;
	}

	void setSalary(int salary)
	{
		this.salary=salary;
	}

	int getSalary()
	{
		return salary;
	}

	void setPension(int pension)
	{
		this.pension=pension;
	}

	int getPension()
	{
		return pension;
	}


}
