package com.fugen.logic;

import java.io.File;
import java.net.URL;

import org.junit.Test;
import org.w3c.dom.Document;


public class MergeXmlDemoTest {

	@Test
	public void testMerge() throws Exception {
		 URL geoData = this.getClass().getResource("geodata.xml");
		URL salaryData = this.getClass().getResource("salarydata.xml");
		File geoDataFile = new File(geoData.getPath());
		File salaryDataFile = new File(salaryData.getPath());
		Document resultDocument = MergeXmlDemo.merge("//person", geoDataFile, salaryDataFile);
		MergeXmlDemo.print(resultDocument);
		MergeXmlDemo.displayElement(resultDocument);
		
		
	}

}
